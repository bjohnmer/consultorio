<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Espera_model extends CI_Model {
	
	function __construct(){
    parent::__construct();
  }

  public function get($data = null, $order = 'id_especialidad', $torder = "DESC")
  {

    if (!empty($data)) {
      
      $data = $this->db->order_by($order, $torder)
                      ->where($data)
                      ->get("tbl_especialidad");
 
    }
    else
    {
      $data = $this->db->order_by($order, $torder)->get("tbl_especialidad");
    }

    if ($data->num_rows()) {
      return $data->result();      
    } else {
      return false;
    }

  }


  // public function getMy($campo, $valor, $user)
  // {
  //   $data = $this->db->where($campo, $valor)->where("id_solicitante",$user)->get("bus_solicitud");
  //   if ($data->num_rows()) {
  //     return $data->result();
  //   } else {
  //     return false;      
  //   }
  // }

  // public function getBy($campo, $valor)
  // {
  //   $data = $this->db->where($campo, $valor)->get("bus_solicitud");
  //   if ($data->num_rows()) {
  //     return $data->result();
  //   } else {
  //     return false;      
  //   }
  // }

  // public function maxNroSolicitudT()
  // {
  //   $data = $this->db->order_by("nro_solicitud_t","DESC")->limit(1)->get("bus_solicitud");
  //   $resultado = $data->result(); 
  //   if ($data->num_rows()) {
  //     return $resultado[0]->nro_solicitud_t;
  //   }
  //   else
  //   {
  //     return false;
  //   }
  // }
  
  // public function create($data)
  // {
    
  //   $solicitud = $this->db->insert('bus_solicitud', $data);
    
  //   if ($solicitud) {
  //     return TRUE;
  //   } else {
  //     return FALSE;
  //   }
  // }

  // public function update($data,$id)
  // {

  //   $solicitud = $this->db->where('id_bus_solicitud', $id)->update('bus_solicitud', $data);
    
  //   if ($solicitud) {
  //     return TRUE;
  //   } else {
  //     return FALSE;
  //   }
  // }

  // public function updateStatus()
  // {

  //   $data = array('status' => "Cerrado");

  //   $solicitud = $this->db->where('CURDATE() > fech_regreso')->update('bus_solicitud', $data);
    
  //   if ($solicitud) {
  //     return TRUE;
  //   } else {
  //     return FALSE;
  //   }
  // }


  // public function getChoferNotIn($fech_salida,$fech_regreso)
  // {

  //   $solicitudes = $this->db
  //                   ->select('id_chofer')
  //                   ->where('("'.$fech_salida.'" BETWEEN fech_salida AND fech_regreso)')
  //                   ->or_where('("'.$fech_regreso.'" BETWEEN fech_salida AND fech_regreso)')
  //                   ->get("bus_solicitud");
  //   foreach ($solicitudes->result() as $row)
  //   {
  //     $arreglo[]=$row->id_chofer;
  //   }
  //   $datos = $this->db
  //               ->where_not_in('id',$arreglo)
  //               ->where("status <> ","Inactivo")
  //               ->get("chofer");
  //   // $sql = $this->db->last_query();
  //   // echo $sql;
  //   if ($datos) {
  //     return $datos->result();
  //   } else {
  //     return FALSE;
  //   }
  // }

  // public function getBusNotIn($fech_salida,$fech_regreso)
  // {

  //   $solicitudes = $this->db
  //                   ->select('id_bus')
  //                   ->where('("'.$fech_salida.'" BETWEEN fech_salida AND fech_regreso)')
  //                   ->or_where('("'.$fech_regreso.'" BETWEEN fech_salida AND fech_regreso)')
  //                   ->get("bus_solicitud");
  //   foreach ($solicitudes->result() as $row)
  //   {
  //     $arreglo[]=$row->id_bus;
  //   }
  //   $datos = $this->db
  //               ->where_not_in('id',$arreglo)
  //               ->where("status <> ","Reparación")
  //               ->where("status <> ","Desincorporado")
  //               ->where("status <> ","Dañado")
  //               ->get("bus");
  //   if ($datos) {
  //     return $datos->result();
  //   } else {
  //     return FALSE;
  //   }
  // }

  // /*
  //   SELECT `id`,`nombre`,`apellido` FROM `chofer` 
  //   WHERE `id` NOT IN (
  //     SELECT `id_chofer` FROM `bus_solicitud` 
  //     WHERE ("2013-05-01" BETWEEN `fech_salida` AND `fech_regreso`) 
  //     AND ("2013-05-07" BETWEEN `fech_salida` AND `fech_regreso`) 
  //     AND `status`="Solicitado")

  // */


  // // public function updateRecaudos($data)
  // // {

  // //   $data_solicitud = array(
  // //                   'comprobante'    => $data['comprobante'],
  // //                   'carta'           => $data['carta'],
  // //                   'informe'        => $data['informe'],
  // //                   'presupuestos'  => $data['presupuestos'],
  // //                   'constancia'    => $data['constancia'],
  // //                   'copiacedula'    => $data['copiacedula'],
  // //                   'status'        => "En Tránsito"
  // //               );

  // //   $solicitud = $this->db->where('id_solicitud_ayuda', $data['id_solicitud_ayuda'])->update('ayuda_solicitud', $data_solicitud);
    
  // //   if ($solicitud) {
  // //     return TRUE;
  // //   } else {
  // //     return FALSE;
  // //   }
  // // }


  // public function destroy($id)
  // {
  //   $data = $this->db->where("id_bus_solicitud", $id)->delete("bus_solicitud");
  //   if ($data) {
  //     return true;
  //   } else {
  //     return false;      
  //   }
  // }

}
