<img class="img-responsive" src="<?=ASSETS_DIR?>img/headerrecipe.png" alt="DEM">
<strong>Fecha: <?=date('Y-m-d')?></strong>
&nbsp;&nbsp;<a href="<?=base_url()?>sesmedico" class="btn btn-success hidden-print"><span class="glyphicon glyphicon-chevron-right">&nbsp;Siguiente Paciente</a>
&nbsp;&nbsp; <a id="imprimir" class="btn btn-success hidden-print"><span class="glyphicon glyphicon-print">&nbsp;Imprimir</a>
&nbsp;&nbsp;<a  href="<?=base_url()?>sesmedico/informe/<?=$paciente[0]->id?>" id="informe" class="btn btn-success hidden-print"><span class="glyphicon glyphicon-book">&nbsp;Informe Médico</a>
<table class="table table-bordered" border="1" width="100%">
  <tr>
    <td witdh="50%"><h2>Récipe</h2></td>
    <td witdh="50%"><h2>Indicaciones</h2></td>
  </tr>
  <tr>
    <td><textarea class="form-control" rows="10" readonly><?=$consulta['recipe']?></textarea></td>
    <td><textarea class="form-control" rows="10" readonly><?=$consulta['indicaciones']?></textarea></td>
  </tr>
  <tr>
    <td><strong>Datos del paciente</strong></td>
    <td><strong>Datos del Médico</strong></td>
  </tr>
  <tr>
    <td>
      <?=$paciente[0]->nombres?> <?=$paciente[0]->apellidos?><br>
      <strong>Historia #:</strong> <?=$paciente[0]->historia?><br>
      <strong>Cédula:</strong> <?=$paciente[0]->cedula?>
    </td>
    <td>
      Dr(a). <?=$medico[0]->nombres?><br>
      <strong>Cédula:</strong> <?=$medico[0]->cedula?><br>
      <strong>Matricula #:</strong> <?=$medico[0]->matricula?><br>
    </td>
    </td>
  </tr>
</table>
<br>
<br>
<br>
<br>
<br>
<br>
<table class="table table-bordered" border="1" width="100%">
  <tr>
    <td witdh="50%"><h2>Exámenes</h2></td>
  </tr>
  <tr>
    <td>
      Glicemia: <?php if (!empty($consulta['glicemia'])) : if ($consulta['glicemia']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Creatinina: <?php if (!empty($consulta['glicemia'])) : if ($consulta['creatinina']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Urea: <?php if (!empty($consulta['urea'])) : if ($consulta['urea']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Trigliceridos: <?php if (!empty($consulta['trigliceridos'])) : if ($consulta['trigliceridos']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Colesterol: <?php if (!empty($consulta['colesterol'])) : if ($consulta['colesterol']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Tsh: <?php if (!empty($consulta['tsh'])) : if ($consulta['tsh']=="Si"): ?>Si<?php endif; endif; ?> <br>
      T3: <?php if (!empty($consulta['t3'])) : if ($consulta['t3']=="Si"): ?>Si<?php endif; endif; ?> <br>
      T4: <?php if (!empty($consulta['t4'])) : if ($consulta['t4']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Orina: <?php if (!empty($consulta['orina'])) : if ($consulta['orina']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Heces: <?php if (!empty($consulta['heces'])) : if ($consulta['heces']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Glicemia: <?php if (!empty($consulta['glicemia'])) : if ($consulta['glicemia']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Tgo: <?php if (!empty($consulta['tgo'])) : if ($consulta['tgo']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Tgp: <?php if (!empty($consulta['tgp'])) : if ($consulta['tgp']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Hierro: <?php if (!empty($consulta['hierro'])) : if ($consulta['hierro']=="Si"): ?>Si<?php endif; endif; ?> <br>
      Calcio: <?php if (!empty($consulta['calcio'])) : if ($consulta['calcio']=="Si"): ?>Si<?php endif; endif; ?> <br>
      <h3>Otros Exámenes:</h3>
      <p>
        <?php if ($consulta['otros']): ?>
          <?=$consulta['otros']?>
        <?php endif ?>
      </p>
    </td>
  </tr>
  <tr>
</table>