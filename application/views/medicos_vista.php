      <div class="row">
        <h3 class="text-center">INFORMACIÓN DE MÉDICOS Y ESPECIALIDADES</h3>
      </div>
      <div class="row">
        <div class="col-lg-4">
          <div class="btn-group-vertical">
            <a href="<?=base_url()?>medicos/nespecialidad" class="btn btn-danger">Nueva Especialidad</a><br>
            <a href="<?=base_url()?>medicos/lespecialidades" class="btn btn-danger">Lista de Especialidades</a><hr>
            <a href="<?=base_url()?>medicos/nmedico" class="btn btn-danger">Nuevo Médico</a><br>
            <a href="<?=base_url()?>medicos/lmedicos" class="btn btn-danger">Lista de Médicos</a><br>
            <a href="<?=base_url()?>medicos/atras" class="btn btn-success"><span class="glyphicon glyphicon-circle-arrow-left"> Atrás</a><br>
          </div>
        </div>
        <div class="col-lg-8">
          <img src="<?=ASSETS_DIR?>img/medicos.png" alt="" class="img-responsive img-menu thumbnail">
        </div>
        <div class="col-lg-4 text-center">&nbsp;&nbsp;&nbsp;</div>
        <div class="col-lg-4"></div>
      </div>
      
      <div class="clearfix separador"></div>
