<img class="img-responsive" src="<?=ASSETS_DIR?>img/headeradmin.png" alt="DEM">
&nbsp;&nbsp;<a href="<?=base_url()?>sesmedico" class="btn btn-success hidden-print"><span class="glyphicon glyphicon-chevron-right">&nbsp;Siguiente Paciente</a>
&nbsp;&nbsp; <a id="imprimir" class="btn btn-success hidden-print"><span class="glyphicon glyphicon-print">&nbsp;Imprimir</a>
<br><br>
<table border="0" align="center" width="70%">
  <tr>
    <td align="right">
      <strong>Fecha: <?=date('d/m/Y')?></strong>
    </td>
  </tr>
  <tr>
    <td>
      <strong>Paciente: </strong><?=$paciente[0]->nombres?> <?=$paciente[0]->apellidos?>
      <br><strong>Cédula: </strong><?=$paciente[0]->cedula?>
      <br><strong>Historia: </strong><?=$paciente[0]->historia?>
    </td>
  </tr>

  <tr>
    <td witdh="50%">
      <h2 class="text-center">Informe Médico</h2>
    </td>
  </tr>

  <tr>
    <td>
      <p style="font-size: 1.2em; text-align: justify;">El/la paciente <strong><?=$paciente[0]->nombres?> <?=$paciente[0]->apellidos?></strong> de <strong><?=$this->datemanager->edad($paciente[0]->fechan);?></strong> años de edad tiene una historia con antecedentes de <strong><?=$consulta[0]->observaciones?></strong> por lo que se indica tratamiento según récipe e indicaciones anexas.</p>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="center">
      Dr(a). <?=$medico[0]->nombres?><br>
      <strong>Cédula:</strong> <?=$medico[0]->cedula?><br>
      <strong>Matricula #:</strong> <?=$medico[0]->matricula?><br>
      <?=$medico[0]->nombre?><br>

    </td>
  </tr>
</table>
<br><br><br>