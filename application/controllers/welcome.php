<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	private $sources;

	function __construct()
	{

		parent::__construct();

		$this->sources = $this->constantes->assets();
		$this->load->model("auth_model");
    $this->load->library("form_validation");
	
	}

	public function index($datos = null, $vista = "index_vista")
	{

		$this->load->view('cabecerafront_vista', $this->sources);
		$this->load->view($vista, $datos);
		$this->load->view('piefront_vista', $this->sources);
	
	}
	public function entrada($datos = null)
	{
		
		$this->index($datos,'entrada_vista');
	
	}
	public function entrar()
	{
	   
	   $this->form_validation->set_rules("usuario", "Usuario", "required|trim|max_length[20]");
	   $this->form_validation->set_rules("clave", "Clave", "required|trim|max_length[20]");

	   if ($this->form_validation->run() == FALSE) 
	   {

	     $this->entrada();

	   } 
	   else 
	   {
	     $clave = sha1(set_value("clave"));
	     $data = $this->auth_model->validar_login(set_value("usuario"), $clave);
	     if ($data) {
	      $this->load->library("session");
	       $this->session->set_userdata("logged_in" , TRUE);
	       $this->session->set_userdata("id",$data->id);
	       $this->session->set_userdata("cedula",$data->cedula);
	       $this->session->set_userdata("nombre",$data->nombre);
	       $this->session->set_userdata("login",$data->login);
	       $this->session->set_userdata("tipo",$data->tipo);
	       if ($data->tipo=='Administrador' || $data->tipo=='Asistente') 
	       {
	       	redirect('admin/');
	       }
	       else if ($data->tipo=='Médico') 
	       {
	       	redirect('sesmedico/');
	       }
	     } 
	     else  
	     {
	       $data['logged_in_fail'] = TRUE;
	       $data['mensaje']['tipo'] = "error";
	       $data['mensaje']['mensaje'] = "Nombre de usuario o clave inválida";
	       $this->entrada($data);
	     }
	   }
		
	}

}