<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Espera extends CI_Controller {
  
  private $sources;
  
  function __construct()
  {
    
    parent::__construct();
    
    $this->load->library('session');
    if (!$this->session->userdata("logged_in")  || !$this->session->userdata("tipo")=="Administrador"  || !$this->session->userdata("tipo")=="Asistente"){
      redirect('/');
    }
    
    $this->sources = $this->constantes->assets();

    $this->load->library("grocery_CRUD");
    $this->load->model("espera_model");

  }
  
  public function mostrar($output = null, $vista = "espera_vista")
  {
  
    $this->sources['css_files'] = $output->css_files;
    $this->load->view('cabeceraadmin_vista', $this->sources);
    $this->load->view($vista, $output);
    $this->sources['js_files'] = $output->js_files;
    $this->load->view('pieadmin_vista', $this->sources);

  }

  public function index()
  {
    
    $pacientes = new grocery_CRUD();
    $pacientes->set_table("espera");
    $pacientes->set_subject("Paciente -> Lista de Espera");
    $pacientes->set_theme("datatables");
    $pacientes->unset_texteditor("observaciones");
    $pacientes->unset_delete();
    $pacientes->where('fecha',date('Y-m-d'));
    $pacientes->field_type('fecha','hidden',date('Y-m-d'));
    $pacientes->field_type('activo','hidden','No');

    $pacientes->set_relation('pacientes_id','pacientes','{cedula} - {nombres} {apellidos} Historia: {historia}',array('pacientes.activo'=>'No'));
    $pacientes->order_by('id','ASC');
    $pacientes->columns("pacientes_id","emergencia","observaciones");
    $pacientes->callback_before_insert(array($this, 'activar_paciente'));

    $pacientes->display_as('pacientes_id', 'Paciente');
    $pacientes->display_as('emergencia', '¿Es Emergencia?');
    $pacientes->set_rules('pacientes_id',"Pacientes",'required');
    $pacientes->set_rules('emergencia',"Emergencia",'required');
    $pacientes->set_rules('observaciones',"Observaciones",'required|min_length[4]');

    $output = $pacientes->render();
    $this->mostrar($output);
  
  }

  function activar_paciente($post_array,$primary_key)
  {
    
    $condiciones = array('activo' => 'Si','fecha' => date('Y-m-d'));
    $hayActivosHoy = $this->db->where($condiciones)->get('espera');
    if(!$hayActivosHoy->result())
    {
      $post_array['activo'] = 'Si';
      $this->db->update('pacientes', array('activo' => 'No') );
      $this->db->update('pacientes', array('activo' => 'Si') , array('id' => $post_array['pacientes_id']) );
    }

    return $post_array;

  }

  public function atras()
  {
    
    redirect("admin");
  
  }

}
