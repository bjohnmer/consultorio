<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pacientes extends CI_Controller {
  
  private $sources;
  
  function __construct()
  {
    
    parent::__construct();
    
    $this->load->library('session');
    if (!$this->session->userdata("logged_in")  || !$this->session->userdata("tipo")=="Administrador" || !$this->session->userdata("tipo")=="Asistente"){
      redirect('/');
    }
    
    $this->sources = $this->constantes->assets();

    $this->load->library("grocery_CRUD");
   
  }
  
  public function mostrar($output = null, $vista = "lpacientes_vista")
  {
  
    $this->sources['css_files'] = $output->css_files;
    $this->load->view('cabeceraadmin_vista', $this->sources);
    $this->load->view($vista, $output);
    $this->sources['js_files'] = $output->js_files;
    $this->load->view('pieadmin_vista', $this->sources);

  }

  public function lpacientes()
  {
    
    $pacientes = new grocery_CRUD();
    $pacientes->set_table("pacientes");
    $pacientes->set_subject("paciente");
    $pacientes->set_theme("datatables");
    $pacientes->columns("historia","cedula","nombres","apellidos");
    $pacientes->field_type('activo','hidden','No');
    
    $pacientes->display_as('historia', 'Historia');
    $pacientes->display_as('cedula', 'Cédula');
    $pacientes->display_as('nombres', 'Nombres');
    $pacientes->display_as('apellidos', 'Apellidos');
    $pacientes->display_as('fechan', 'Fecha de Nacimiento');
    $pacientes->display_as('teleh', 'Teléfono de Habitación');
    $pacientes->display_as('telecelu', 'Teléfono Móvil');
    $pacientes->display_as('estadoc', 'Estado Civil');

    $pacientes->set_rules('historia',"Historia",'required|numeric|min_length[6]|max_length[12]');
    $pacientes->set_rules('cedula',"Cédula",'required|numeric|min_length[6]|max_length[12]');
    $pacientes->set_rules('nombres',"Nombres",'required|alpha_space|min_length[2]|max_length[75]');
    $pacientes->set_rules('apellidos',"Apellidos",'required|alpha_space|min_length[2]|max_length[75]');
    $pacientes->set_rules('estadoc',"Estado Civil",'required');
    $pacientes->set_rules('sexo',"Sexo",'required');

    $output = $pacientes->render();
    $this->mostrar($output);
  
  }
  public function consultas()
  {
    $consultas = new grocery_CRUD();
    $consultas->set_table("consultas");
    $consultas->set_subject("Consulta");
    $consultas->set_relation('medico_id','medicos','{cedula} - {nombres}');
    $consultas->set_relation('paciente_id','pacientes','{cedula} - {nombres} {apellidos}');
    $consultas->set_theme("datatables");
    $consultas->columns('fechaconsul','paciente_id','medico_id','motivo');
    $consultas->display_as('fechaconsul','Fecha de Consulta');
    $consultas->display_as('diagnostico','Diagnóstico');
    $consultas->display_as('medico_id','Médico');
    $consultas->display_as('paciente_id','Paciente');
    $consultas->unset_add();
    $consultas->unset_delete();
    $consultas->unset_edit();
    $output = $consultas->render();
    $this->mostrar($output,'lconsultas_vista');
  }
  
  public function reposos()
  {
    $reposos = new grocery_CRUD();
    $reposos->set_table("reposos");
    $reposos->set_subject("Reposo");
    $reposos->set_relation('medicos_id','medicos','{cedula} - {nombres}');
    $reposos->set_relation('paciente_id','pacientes','{cedula} - {nombres} {apellidos}');
    $reposos->set_theme("datatables");
    $reposos->columns('fechareposo','paciente_id','medico_id','motivo', 'desde','hasta');
    $reposos->display_as('fechareposo','Fecha de Consulta');
    $reposos->display_as('medico_id','Médico');
    $reposos->display_as('paciente_id','Paciente');
    $reposos->unset_add();
    $reposos->unset_delete();
    $reposos->unset_edit();
    $output = $reposos->render();
    $this->mostrar($output,'lreposos_vista');
  }
  public function npaciente()
  {
    
    redirect("pacientes/lpacientes/add");
  
  }

  public function atras()
  {
    
    redirect("admin");
  
  }

}
