USM
===
(Unidad de Servicio Médicos)
(en desarrollo)

Aplicación Web para el Control de la Unidad de Servicios Médicos de la DAR TRUJILLO

Asesoría: Diseño, Desarrollo e Ingeniería de Software Web

Herramientas utilizadas:

Front-end: HTML5, CSS3, Framework CSS Twitter Bootstrap (http://getbootstrap.com)

Back-end: PHP, Framework PHP Codeigniter (http://ellislab.com/codeigniter), Librería GroceryCRUD (http://www.grocerycrud.com/)

Base de Datos: MySQL