-- phpMyAdmin SQL Dump
-- version 4.0.6deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 16, 2013 at 04:06 PM
-- Server version: 5.5.34-0ubuntu0.13.10.1
-- PHP Version: 5.5.3-1ubuntu2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `usm`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`) VALUES
('c541845520fb602f6ba3bd31b81e93df', '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:25.0) Gecko/20100101 Firefox/25.0', 1384456201, 'a:7:{s:9:"user_data";s:0:"";s:9:"logged_in";b:1;s:2:"id";s:1:"1";s:6:"cedula";s:9:"123456789";s:6:"nombre";s:4:"root";s:5:"login";s:4:"root";s:4:"tipo";s:13:"Administrador";}');

-- --------------------------------------------------------

--
-- Table structure for table `consultas`
--

CREATE TABLE IF NOT EXISTS `consultas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fechaconsul` date NOT NULL,
  `motivo` varchar(255) NOT NULL,
  `recipe` text NOT NULL,
  `indicaciones` text NOT NULL,
  `diagnostico` text NOT NULL,
  `observaciones` text,
  `glicemia` enum('Si','No') DEFAULT 'No',
  `creatinina` enum('Si','No') DEFAULT 'No',
  `urea` enum('Si','No') DEFAULT 'No',
  `trigliceridos` enum('Si','No') DEFAULT 'No',
  `colesterol` enum('Si','No') DEFAULT 'No',
  `tsh` enum('Si','No') DEFAULT 'No',
  `t3` enum('Si','No') DEFAULT 'No',
  `t4` enum('Si','No') DEFAULT 'No',
  `orina` enum('Si','No') DEFAULT 'No',
  `heces` enum('Si','No') DEFAULT 'No',
  `tgo` enum('Si','No') DEFAULT 'No',
  `tgp` enum('Si','No') DEFAULT 'No',
  `hierro` enum('Si','No') DEFAULT 'No',
  `calcio` enum('Si','No') DEFAULT 'No',
  `otros` text,
  `medico_id` int(11) NOT NULL,
  `paciente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`medico_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `consultas`
--

INSERT INTO `consultas` (`id`, `fechaconsul`, `motivo`, `recipe`, `indicaciones`, `diagnostico`, `observaciones`, `glicemia`, `creatinina`, `urea`, `trigliceridos`, `colesterol`, `tsh`, `t3`, `t4`, `orina`, `heces`, `tgo`, `tgp`, `hierro`, `calcio`, `otros`, `medico_id`, `paciente_id`) VALUES
(2, '2013-09-01', 'kjshd fkjhs dkjfh ksd', 'jh fksdjh fkjshdfkjskjhfk', 'lksj flkdsj lfkjsdlkfjsldkjflksd', 'lksjdflkjsd lkfjsldkjflksdjl fs', 'lksdj flkdjs lkfj sdlkjf lksj flkjsl', 'Si', 'No', '', 'No', 'No', 'No', 'No', 'No', 'Si', 'Si', 'No', 'No', 'Si', 'Si', '', 1, 2),
(3, '2013-09-01', 'kjh skjfh ksjhf ksdf', 'jhs kfhs khf kshfkhskf', 'ksjdfh kjsh fkjsh kfhsk hfk', 'sdjfhskuhfk sjhf ksh kfh skf', '', 'Si', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 3),
(4, '2013-09-01', 'jhs dfkjsdh fkjhs dkfsd', 'SKJD FHKJSH FKJSH KFH', 'KJSLDKF JLSKDJ FLKS', 'LKAJ DLJSA L', 'LKJ SDLKAJ LDKJ', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 4),
(5, '2013-09-01', 'jhgf kjsdhf kjsdhkfjhsdk fs', 'IUYEWIR ,N DFKSJH K', 'JH KJH KSDHFK DSHFK', 'KJHDS KFJHS DKFJH SDK', 'KJHD KASJHD KAS', 'Si', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 1),
(6, '2013-09-01', 'kj lsdkjfl sjflj slf', 'lsjdfl sldfj lskdj fljksdljflsd', 'hk sdjfhkj shkfhsdkh', 'kdjf glkjd lgjldkj gljd lkgj ld', 'lkjs lkfj sdlkjf lskdj flkjs lfs', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 1),
(7, '2013-09-01', 'kjsdfh kjhf kjsdh fksd', 'kjh akdh kjahd ka', 'kajshd kjahd kjahks', 'kjhasdkjha kjdhakjhd k', 'kajh dkjash dkjah kjdh ak', 'Si', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(8, '2013-09-01', 'kjhd fkjsdhf kjdshfk hs dkf', 'kjh fkjsh fkjhs kfjhs kjhf kjsh kfh', 'kjh kasjhd kjahdiytuyewgr mbv mnbv xjy uyg jg rjhg wjhr g', 'kjhfiywtef bmnsbf shgf jysgfjhgsd fjhgs fjhg suyft wjehf jshf jhsg fjhg sjhg jh', 'jhsgfuyfwge jfhb djshgf sh gfjhstfjwehg mnrb fm aa', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(9, '2013-09-01', 'kjhf ksjdhf ksh fkjs kjfhksdkfs', 'kjs dlfkj slfj lksjl kfjlsk jflkj lskjfls', 'lkj slkjf lksj lfkjslkjflksjlkfjlksdjf lk', 'lkjf lksj flkjslkf jlksjflksiterkjthnkjg dfkjhg dfkjhk', 'iuyrelkjgkdjf h kjhjf jhasg hg fdj', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 3),
(10, '2013-09-02', 'jh kasjhdk ajhdkjh akhd kjahkdjh kajh dka', 'kjsdh fkdsjh fkjsdh fkjhsdk fhskjhf ksjh fkj', 'kjhf skjhf sdkjhf kjsdh fkjhsdk fhksjdh fkjhsd kj', 'jh fkdsajhd kajdh kjahd kjsahd kjash dkja', 'kjh kjsahd ksjah dkjah kdjhakj dhkjah dk', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(11, '2013-09-02', 'kjsdh fkjhsf kjsh kfjhs kjfh sdkjh fkjsh fkj hskf ', 'lkj lksjf lkjsflkjslkjf lksj flksjlkf slk', 'lskjf lksj flksj flkjs lkfj lsjfljslkfj slkdjf lksj flksd', 'lksjf lksj flksj lkfj sdljf lksjf lksdjl', 'Lorem ipsum dolor sit amet, Jose Araujo consectetur adipiscing elit. Nulla mi sem, gravida at imperdiet a, accumsan et neque. Cras enim mauris, vehicula ut diam at, egestas pharetra nisi. Integer bibendum est quis tellus varius, et tincidunt neque iaculis. Maecenas fermentum ante ligula, ac ultrices enim lacinia sed. Fusce pellentesque ut risus vitae consectetur. Vivamus gravida leo vitae elit aliquam sodales. Curabitur ac lectus arcu. In egestas dignissim tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque venenatis at tortor et vulputate. ', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 3),
(12, '2013-09-02', 'kjash kjsah dkjha kjdh sakjh kjash kd a', 'lksdj flkjs flkjs lkfj slkjflksdj flksd', 'lkj sflksdj flksj flksdj lfkjslkfjslkjfls jl', 'lskj flksj flkjs lkfj slkjf lksj flkjs lfkj slkj', 'Lorem ipsum dolor sit amet, Juan Carlos Pérez consectetur adipiscing elit. Nulla mi sem, gravida at imperdiet a, accumsan et neque. Cras enim mauris, vehicula ut diam at, egestas pharetra nisi. Integer bibendum est quis tellus varius, et tincidunt neque iaculis. Maecenas fermentum ante ligula, ac ultrices enim lacinia sed. Fusce pellentesque ut risus vitae consectetur. Vivamus gravida leo vitae elit aliquam sodales. Curabitur ac lectus arcu. In egestas dignissim tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque venenatis at tortor et vulputate. ', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(13, '2013-09-02', 'kjash kjsah dkjha kjdh sakjh kjash kd a', 'lksdj flkjs flkjs lkfj slkjflksdj flksd', 'lkj sflksdj flksj flksdj lfkjslkfjslkjfls jl', 'lskj flksj flkjs lkfj slkjf lksj flkjs lfkj slkj', 'Lorem ipsum dolor sit amet, Juan Carlos Pérez consectetur adipiscing elit. Nulla mi sem, gravida at imperdiet a, accumsan et neque. Cras enim mauris, vehicula ut diam at, egestas pharetra nisi. Integer bibendum est quis tellus varius, et tincidunt neque iaculis. Maecenas fermentum ante ligula, ac ultrices enim lacinia sed. Fusce pellentesque ut risus vitae consectetur. Vivamus gravida leo vitae elit aliquam sodales. Curabitur ac lectus arcu. In egestas dignissim tincidunt. Interdum et malesuada fames ac ante ipsum primis in faucibus. Pellentesque venenatis at tortor et vulputate. ', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(14, '2013-09-07', 'kjhsd kfjh skjfhkshfksh fksjh fkjhskfhskhfk sh fkjds', 'dhg jsdhg fjhsgjfdsf\nshdbf jhsdg fjhdsjf\nmhsd fhjd sjhf', 'jhsdg fjhsdg fjhgsdfjhgj shd\njhsdg fjhgd sjf\njhsdg fjhgds jhfgjs', 'jhgdsjhgdsfj hg jg sjdgf jgs jfg jshgfjhhgsjgf jhsg fjgs jhgf jhsg jhfgsjhgf', 'ksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjds ksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjdsksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjdsksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjdsksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjdsksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjdsksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjdsksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjdsksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjdsksd  fjh gsjhfg jsg fjhgs jfhg sjhgf jhsg fjhg sjhgf jdsg jfgsdjhgfjds', 'Si', 'No', '', 'No', 'No', 'No', 'No', 'No', 'Si', 'Si', 'No', 'No', '', '', '', 1, 1),
(15, '2013-09-07', 'kjhsd fkjhsa fklj hsakljf hlkjsha fkls af', 'jhg jhgd ahsjg djhasg djhga jhgd jag djga jdgjash', 'jgasd jhasg djhag jdhg asjhgdja', 'JHGJHASG DJHGA HJDGJHAJH DJHSAH DKJHA KJHDAKHKD', 'OBSERVACIONES hgasjdhgsa jdg ajhgd jhga jhdg ajhgdjhgas j OBSERVACIONES hgasjdhgsa jdg ajhgd jhga jhdg ajhgdjhgas jOBSERVACIONES hgasjdhgsa jdg ajhgd jhga jhdg ajhgdjhgas jOBSERVACIONES hgasjdhgsa jdg ajhgd jhga jhdg ajhgdjhgas jOBSERVACIONES hgasjdhgsa jdg ajhgd jhga jhdg ajhgdjhgas jOBSERVACIONES hgasjdhgsa jdg ajhgd jhga jhdg ajhgdjhgas jOBSERVACIONES hgasjdhgsa jdg ajhgd jhga jhdg ajhgdjhgas jOBSERVACIONES hgasjdhgsa jdg ajhgd jhga jhdg ajhgdjhgas jOBSERVACIONES hgasjdhgsa jdg ajhgd jhga jhdg ajhgdjhgas j', 'Si', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(16, '2013-09-07', 'kjhsd fkjhsa fklj hsakljf hlkjsha fkls af', 'jhsdg fjhsgd fjhgsd jfg jsg fjhsdj', 'jhsgd jhags djhas jdgjaskhfkjdsh kjfhsk fs', 'kjhds kfjhds kjhkjshkfjh ksjhfk ds k', 'jdsh kjjsh dfkjjhskjhfkshfkjhsdk fsd', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(17, '2013-09-07', 'kjhsd fkjhsa fklj hsakljf hlkjsha fkls af', 'jsd kjfsdkjjf ksjhkjfh sk hjf kh skfsdk', 'b dsb fbsjhdfjhsdgfjh gds jfgsdjhg', 'hjsgd fjhgd sjhfgsdjgf jdshg fjgds', 'bsd fjhgds jhfgs jhgf jhsg fjsdg jfgsdj', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(18, '2013-09-07', 'kjhsd fkjhsa fklj hsakljf hlkjsha fkls af', 'jbfj shdgf jhgs fjgs jd', 'ghsd jfg sjhgf jhsg jfsd', 'kjsdh fkjhs kjfh ksjd', 'kkjhsd fkjhs kdhjfs', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(19, '2013-09-07', 'kjhsd fkjhsa fklj hsakljf hlkjsha fkls af', 'sjdhgf jhsdgfjhhgs djfg sdj gfjsdgfd jfgj', 'hg dsjhgf sdhjgf jshgfjgsdj', 'hjgsd fjhgsd fjhg sdjgf jhsgd jfs', 'g jhdsg fjhdsgfj hsdg jhfsd', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(20, '2013-09-07', 'jsdgfjhdsg fjhgsdjfgdsj fgsjd gfjsdg jd', 'jhgdfjhdsgjfhhgsd jfg sj gfjsg jfgsdjgf sdj', 'v jhag djhag jdhgajhgdjagj', 'jhgjhgf sjhhgf jhsgfjsgjf gsj', 'jhg jhsg dhjfgsj gfjsd', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 1),
(21, '2013-09-07', 'h fdsjhgf jhsg fjhsg jfg dsjsfd', 'jhgs djfhgjshg fjhgs jhgf jhsgjhfgsdj', 'bvds svd fhv sdjhgfsjhgdj sd', 'jhgs dfjhgs jhfg shjdg fjgsd j', 'jhgd jhgs jhfg sjhgf jdsgj djsf', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(22, '2013-09-07', 'jhgs fjhdshg fjhgdsjhfg sdjgsd', 'sdbv jfhdbv sjhgfsjhgfjsgjsgd jfgs', 'nv nbdsv fjv sdjfvdsjvfjhdsgjs', 'jhsgd fjhdsgf jhsgjfhgdsj', 'jhgs jhgasjhdgajhgdjagdjgasjdhg ajhgd jahg djhgasj', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 3),
(23, '2013-09-07', 'jhg fjhgd sjfhd sjdfs', 'jhdgsf jhsg fhgsj fg sdjfsd', 'hjgd jhsgf jhsg fjgsj fsd', 'jhdg sjhdsg fjhsg fjsd', 'h jhsgd fjh sgdjhfgsdjgfsdjg', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 2),
(24, '2013-09-07', 'jhg fjhsdg fjhsgjhf sdj j', 'nbvds nbfvs nbvf nsbvnfsd', 'nbvnbvsdnbvnsdbv nsvdnbfvsdn', 'nbvdnbv dsnbv nbsv nbvsdnvfdsb', 'nbv dnbvfndsbvnbvds nfdsn', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', '', 1, 4),
(25, '2013-10-28', 'kjhs fkshf kjshf kjsh fkh dsksfd ', 'kjh kajhd kajhd kjha skd hakjh dkjha kd ka', 'jb jhbdjhas djah djha jsja', 'hasg jhsgd jhag djag jdhhg ajhgd jha gdjhg ajg daj', 'jah djahhgd jahgd jag djhajhdg aj gdjgjdgajhgh dja d ajg d ad aj djh ajd aj jah djahhgd jahgd jag djhajhdg aj gdjgjdgajhgh dja d ajg d ad aj djh ajd aj jah djahhgd jahgd jag djhajhdg aj gdjgjdgajhgh dja d ajg d ad aj djh ajd aj jah djahhgd jahgd jag djhajhdg aj gdjgjdgajhgh dja d ajg d ad aj djh ajd aj jah djahhgd jahgd jag djhajhdg aj gdjgjdgajhgh dja d ajg d ad aj djh ajd aj jah djahhgd jahgd jag djhajhdg aj gdjgjdgajhgh dja d ajg d ad aj djh ajd aj jah djahhgd jahgd jag djhajhdg aj gdjgjdgajhgh dja d ajg d ad aj djh ajd aj', 'Si', 'No', 'Si', 'No', 'No', 'No', 'No', 'No', 'Si', 'Si', 'No', 'No', '', '', 'kjf ksjdh fkjsh fkjjh skjhf ksjh fkjh skjhf skhfkjhs kjfh skjjhf ksh fks ', 2, 2),
(26, '2013-10-28', 'masnlkdaj slkj alkkjdkaj dlkjaldjladjkad jalkdaj lkda', 'jhgf jshg fjhsg fjghs jfhg sjhgf jhsg jfhgsj gf js', 'jhsg fjhsg fjhsg fjhgs jhfgjhsgfjhgsjfg jhsg jfhhg sjhg fjsg jfs', 'jg jshg fjhsg fjhgshjgf jhsg fjhgs jfhg sjhgf jhsg jfhds', 'jhgf dshjg fjhsg fjhgs jhfg sjhg fjhgs jhfg sjhg fjhgs jhfgdsj', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', 'b jdhg ajhgd jhag djhg ajhgd jhag djga jhdg jhagd jhga jhdg jahgd jhga jhdg asj gdjajd gja', 1, 1),
(27, '2013-10-28', 'kjah dkjah dkjjhakjdhkah dkjja kjjd hkjah dk hak', 'jha kdh kajhd kjha kjdh kajh dkjh akjhd kjah kdjhakjdh kas', 'jba ksjdh kjahd kjjhak  dhkajhdkjha kjdh kajh dkjhakjhd kjah dkjhak', 'hd kjahd kjah kjdhakjhdkjah dkhakjhd kahd kah kdah', 'kjhas kdjhakjhd kjahkdjhak jhd kjahd kjha dkjhas lkduytr jqghfkjhgf jkg fkjasg khgf jhhbc ncb  jhgckjhg ckjhgkjhg kjshg kjhsg cjhhgsckjh gsjhc sh cjh sjcgjhsgc jkh schj sjhg cjhsga chj gsjahg ckhjgsak jhcgywetyew kjhg kjbcmn xzbvcmnvc nmbv cnzbv cjkhsgkhcgjhcg jhcg skjghc kjhgkj tuytj h mnbv nbdd nsabd snbvds anbf  f sf sjkfgjhsg fjhg fh sjhgf khgs fjhg sajh', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', 'h dksjf kj fksjfkjhs lkjfh klshf dkjsh fkjh fks', 1, 2),
(28, '2013-10-28', 'jhas djhag djhag jhdg ajhg djhagjajgd ajg dja', 'jhs kjhs fkjhsdkjfhskhf k', 'kjh skjjdhfkjhskjfhdskj hk', 'jdhf dskjjhf dskjhfkjsdhfkjhsdkjfh sdkhf kdsk sd', '', '', 'No', '', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', 'Si', 'kjhsdd skajjh dsakjh dkja ks', 1, 3),
(29, '2013-10-28', 'jhgd jahg djhag jhg ajhg dajhg djh gajdg ajhgd ja j', 'kjhadkjh dkjah dkjahdkjha kjdh ska', 'kjah dkajhd kjahdkjjha kdjhakj hdja hkd hakj dk adkj hakj dkj akjd hak hdkj hakjdhak dkjahdkhak dha k', 'jhgasd jhg djhag djhag djhg ajhhgdajhhg djhag dh gaj gd jagjdgjah gdjh ajd', 'hasg djhahgd jag djhgha dhga jhdgjahgd jhga jdhg ajhg djhgajd gjadj', '', 'No', 'Si', 'No', 'No', 'No', 'No', 'No', '', '', 'No', 'No', '', '', 'asjd lkaj dlkja dlkja lkdj alkjd lkaj dlkjas', 1, 4);

-- --------------------------------------------------------

--
-- Table structure for table `especialidades`
--

CREATE TABLE IF NOT EXISTS `especialidades` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(75) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `especialidades`
--

INSERT INTO `especialidades` (`id`, `nombre`) VALUES
(1, 'Medicina Interna'),
(2, 'Ginecología'),
(3, 'Odontología'),
(4, 'Medicina General');

-- --------------------------------------------------------

--
-- Table structure for table `espera`
--

CREATE TABLE IF NOT EXISTS `espera` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `pacientes_id` int(11) NOT NULL,
  `emergencia` enum('Si','No') NOT NULL DEFAULT 'No',
  `observaciones` text NOT NULL,
  `activo` enum('Si','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `medicos`
--

CREATE TABLE IF NOT EXISTS `medicos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` int(11) NOT NULL,
  `matricula` varchar(10) NOT NULL,
  `nombres` varchar(100) NOT NULL,
  `especialidad_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cedula` (`cedula`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `medicos`
--

INSERT INTO `medicos` (`id`, `cedula`, `matricula`, `nombres`, `especialidad_id`) VALUES
(1, 42134231, '423413432', 'Juan González', 1),
(2, 42443232, '76543234', 'Carlos Pérez', 3);

-- --------------------------------------------------------

--
-- Table structure for table `pacientes`
--

CREATE TABLE IF NOT EXISTS `pacientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `historia` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombres` varchar(75) NOT NULL,
  `apellidos` varchar(75) NOT NULL,
  `fechan` date NOT NULL,
  `teleh` varchar(25) NOT NULL,
  `telecelu` varchar(25) NOT NULL,
  `estadoc` enum('Soltero','Casado','Divorciado','Viudo') NOT NULL DEFAULT 'Soltero',
  `sexo` enum('Masculino','Femenino') NOT NULL DEFAULT 'Masculino',
  `activo` enum('Si','No') NOT NULL DEFAULT 'No',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pacientes`
--

INSERT INTO `pacientes` (`id`, `historia`, `cedula`, `nombres`, `apellidos`, `fechan`, `teleh`, `telecelu`, `estadoc`, `sexo`, `activo`) VALUES
(1, 44234242, 12354345, 'Johnathan', 'Cabrera', '1978-08-16', '652476527', '437625472', 'Soltero', 'Masculino', 'No'),
(2, 4343231, 15343432, 'Juan Carlos', 'Pérez', '1980-02-13', '123432432', '123423432', 'Soltero', 'Masculino', 'No'),
(3, 123123, 231231231, 'Jose', 'Araujo', '1994-08-18', '12432141', '124321413', 'Soltero', 'Masculino', 'No'),
(4, 45547654, 345646, 'Luisa', 'Garcia', '1984-11-16', '7547', '45675', 'Casado', 'Femenino', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `reposos`
--

CREATE TABLE IF NOT EXISTS `reposos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fechareposo` date NOT NULL,
  `motivo` text NOT NULL,
  `desde` date NOT NULL,
  `hasta` date NOT NULL,
  `diagnostico` text NOT NULL,
  `observaciones` text NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `medicos_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `reposos`
--

INSERT INTO `reposos` (`id`, `fechareposo`, `motivo`, `desde`, `hasta`, `diagnostico`, `observaciones`, `paciente_id`, `medicos_id`) VALUES
(1, '2013-09-01', 'jh kjhsd fkhs kfks hkfh skhkf', '2013-09-02', '2013-09-10', 'kjsd flksj flkjsl fjlksj lfj slj fls', 'ksjd fhkjshf kjsh kfh skfkjsd', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(40) NOT NULL,
  `login` varchar(20) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `confirmar` varchar(50) NOT NULL,
  `tipo` enum('Administrador','Médico','Asistente') DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `usuarios`
--

INSERT INTO `usuarios` (`id`, `cedula`, `nombre`, `login`, `clave`, `confirmar`, `tipo`) VALUES
(1, 123456789, 'root', 'root', 'dc76e9f0c0006e8f919e0c515c66dbba3982f785', '', 'Administrador'),
(3, 42134231, 'Juan Gonzalez', 'jgonzalez', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Médico'),
(4, 42443232, 'Carlos Pérez', 'cperez', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'Asistente');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
